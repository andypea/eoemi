
Downloaded from https://philharmonia.co.uk/resources/sound-samples/

> Licensing
>
> You are free to use these samples as you wish, including releasing them as part of a commercial work. The only restriction is that they must not be sold or made available ‘as is’ (i.e. as samples or as a sampler instrument).
