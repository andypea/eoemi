---
title: NetWord
category: note
description: >
  Yet another word game.
image: img/observable.png
tags: 
  - note
  - post
layout: article.njk
css: [ "/css/chordinator.css" ]
---

# {{ title }}

<iframe width="100%" height="1020" frameborder="0"
  src="https://observablehq.com/embed/@andypea/netword?cells=chart2%2Ctitle%2Cinstructions%2Cstatus%2CscoreCard%2Cviewof+guessBox2%2CshareCard%2Cviewof+shareButton%2CshareResponse%2Cfeedback"></iframe>

