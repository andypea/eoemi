---
title: Hank Drum
category: instrument
tags: 
  - instrument
  - post
image: /img/Blue_Rhino_grill_tank.jpg
description: >
  A hank drum is a type of metal tongue drum, designed by Dennis 
  Havlena. They can be made out of a 9kg BBQ propane tank.
oscilator:
  - idiophone
  - bar
  - clamped bar
  - steel
layout: article.njk
designer: Dennis Havlena
date: 2021-06-04
---

# {{ title }}

![{{ title }}]({{ image | url }})

{{ description }}

- [Original Design](http://www.dennishavlena.com/for-webpage-lp-hang.htm)
- [Lower Pitched Design](http://www.dennishavlena.com/halohank.htm)
