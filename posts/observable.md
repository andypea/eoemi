---
title: Observable
category: note
description: >
  Observable is a great website that can be used to create interactive
  data visualisations. The visualisations it produces can also be
  exported and embedded into other websites.
image: img/observable.png
tags: 
  - note
  - post
layout: article.njk
---

# {{ title }}

<div id="observablehq-c7c68ce6"></div>
<p>Credit: <a href="https://observablehq.com/d/36690038860b2d65">Steady-state Amplitude by andrew-primer-e</a></p>

<script type="module">
import {Runtime, Inspector} from "https://cdn.jsdelivr.net/npm/@observablehq/runtime@4/dist/runtime.js";
import define from "https://api.observablehq.com/d/36690038860b2d65.js?v=3";
new Runtime().module(define, Inspector.into("#observablehq-c7c68ce6"));
</script>

