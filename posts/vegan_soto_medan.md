---
title: Vegan Soto Medan
description: >
  An attempt to replicate the delicious Indonesian soup from my
  favorite restaurant, Kuta Bali.
image: img/lemongrass.jpg
category: cooking 
tags: 
  - note
  - post
layout: article.njk
---

# {{ title }}

Adapted from the recipe at: <http://tastyeasyhealthy.blogspot.com/2008/07/soto-medan.html>.

## Ingredients (serve 6)
- 500g tofu
- 500g vegetables
- 1 bay leaf
- 1 tbsp lemon grass
- 5 kafir lime leaves
- 3cm galangal
- 300ml coconut milk
- 6 cups bean sprout (boil quickly before serving)
- 2 tbsp soy sauce
- 1 tsp salt
- 1 tbsp kecap manis

### Spices
- 8 shallots
- 3 cloves garlic
- 1/2 tsp pepper
- 1/2 tbsp ground coriander seed
- 1/2 tsp turmeric
- 3cm ginger
- 3 nutmeg

### Garnish
- 1 green onion
- fried shallots

## Cooking instructions

1. Grind all the spices into a paste and fry for a minute or two. 
3. Put one litre of water in a saucepan; then add the ground spices, salt, sweet soy sauce, bay leaf, lime leaves, coconut milk and galangal.
4. Simmer the mixture whilst stirring occasionaly for about one hour until the coconut milk has mixed well.
5. Add the tofu and vegetables to the mixture and heat through.
6. Place one cup of bean sprouts in a bowl, pour over the soup and garnish with green onion and fried shallots.
