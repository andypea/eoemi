---
title: Markdown Examples
category: note
description: >
  This website uses KaTeX to render latex equations and 
  eleventy-plugin-syntaxhighlight for syntax highlighting.
image: img/markdown.jpg
tags: 
  - note
  - post
layout: article.njk
---

# {{ title }}

Here are some equations, written in $\LaTeX$ and formatted by $\KaTeX$ ($x = y^2$):

$$
\frac{a}{b} = c
$$

And here is some code with syntax highlighting:

<!-- Markdown Template -->
``` js
function myFunction() {
  return true;
}
```

## Bullet Points

  - I’m in a bullet point; and
  - so am I; and
  - I am as well!
