---
title: Musical Instrument Resources 
description: >
  There are many great websites and books devoted to the creation
  of musical instruments.
image: img/musical_instruments.jpg
category: note
tags: 
  - note
  - post
layout: article.njk
---

# {{ title }}

## Musical Instrument Design

### Websites

* [Bart Hopkin's Website](http://barthopkin.com/instrumentarium/)
* [Dennis Havlena's Website](http://www.dennishavlena.com/)
* [Odd Music](http://www.oddmusic.com/)

### Books

* [Musical Instrument Design](http://barthopkin.com/books-cds/musical-instrument-design-2/) by Bart Hopkin
* [Acoustics for violin and guitar makers.](http://www.speech.kth.se/music/acviguit4/index.html) by Erik Jansson

### Journals

* [Experimental Musical Instruments](https://archive.org/details/emi_archive/1-START%20HERE/) (with [index](http://barthopkin.com/experimental-musical-instruments-back-issues/))

## Musical Theory and Accoustics

### Websites

* [Music acoustics at UNSW](https://newt.phys.unsw.edu.au/jw/SiteMap.html)
* [KTH - Division of Speech, Music and Hearing](https://www.kth.se/is/tmh/)
* [Insightful HackerNews Comment](https://news.ycombinator.com/item?id=27589614)

### Papers

* [Tonal Consonance and Critical Bandwidth (1965)](https://doi.org/10.1121/1.1909741) by R. Plomp and W. J. M. Levelt

### Books

* [The Physics of Musical Instruments](https://www.springer.com/gp/book/9780387983745) by Fletcher and Rossing

## Samples Libraries

  * [Philharmonia (including Guitar)](https://philharmonia.co.uk/resources/sound-samples/)
