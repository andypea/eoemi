---
title: Chordinator
category: note
description: >
  An interactive guide to chord fingerings on guitar and bass.
image: img/observable.png
tags: 
  - note
  - post
layout: article.njk
css: [ "/css/chordinator.css" ]
---

# {{ title }}

<div id="chordinator">
<div id="observablehq-viewof-rootStep-e314e9cf"></div>
<div id="observablehq-viewof-rootAlter-e314e9cf"></div>

<div id="observablehq-viewof-chord-e314e9cf"></div>

<div id="observablehq-viewof-lowestFret-e314e9cf"></div>
<div id="observablehq-viewof-numFrets-e314e9cf"></div>

<div id="play-buttons">
<div id="observablehq-playChordButton-e314e9cf"></div>
<div id="observablehq-playNotesButton-e314e9cf"></div>
</div>

<div id="observablehq-chart-e314e9cf"></div>
</div>

<script type="module">
import {Runtime, Inspector} from "https://cdn.jsdelivr.net/npm/@observablehq/runtime@4/dist/runtime.js";
import define from "https://api.observablehq.com/@andypea/chordinator.js?v=3";
new Runtime().module(define, name => {
  if (name === "chart") return new Inspector(document.querySelector("#observablehq-chart-e314e9cf"));
  if (name === "playChordButton") return new Inspector(document.querySelector("#observablehq-playChordButton-e314e9cf"));
  if (name === "playNotesButton") return new Inspector(document.querySelector("#observablehq-playNotesButton-e314e9cf"));
  if (name === "viewof chord") return new Inspector(document.querySelector("#observablehq-viewof-chord-e314e9cf"));
  if (name === "viewof rootAlter") return new Inspector(document.querySelector("#observablehq-viewof-rootAlter-e314e9cf"));
  if (name === "viewof rootStep") return new Inspector(document.querySelector("#observablehq-viewof-rootStep-e314e9cf"));
  if (name === "viewof lowestFret") return new Inspector(document.querySelector("#observablehq-viewof-lowestFret-e314e9cf"));
  if (name === "viewof numFrets") return new Inspector(document.querySelector("#observablehq-viewof-numFrets-e314e9cf"));
  return ["longSymbol","chordNotes","fretNotes","rootNote","fretExtent","height","yScale","frets"].includes(name);
});
</script>

