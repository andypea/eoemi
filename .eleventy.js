let markdownIt = require("markdown-it");
let markdownItKatex = require('@iktakahiro/markdown-it-katex');

let options = {
  html: true,
  breaks: false,
  linkify: true
};

let markdownLib = markdownIt(options).use(markdownItKatex);

const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const Image = require("@11ty/eleventy-img");
const path = require('path')

async function imageShortcode(src, alt, sizes) {
  let metadata = await Image(src, {
    widths: [300, 600],
    formats: ["avif", "jpeg"]
  });

  let imageAttributes = {
    alt,
    sizes,
    loading: "lazy",
    decoding: "async",
  };

  // You bet we throw an error on missing alt in `imageAttributes` (alt="" works okay)
  return Image.generateHTML(metadata, imageAttributes, {
    whitespaceMode: "inline"
  });
}

module.exports = function(eleventyConfig) {
  eleventyConfig.addNunjucksAsyncShortcode("image", imageShortcode);
  eleventyConfig.addLiquidShortcode("image", imageShortcode);
  eleventyConfig.addJavaScriptFunction("image", imageShortcode);

  eleventyConfig.setLibrary("md", markdownLib);
  eleventyConfig.addPlugin(syntaxHighlight);

  //
  // Custom Filters that can be used in templates.
  //
  // TODO: Should these filters copy the sequence before possibly modifying it?

  // Extracts the attr from each element of sequence.
  eleventyConfig.addFilter("extractAttr", function(sequence, attr) {
    return sequence.map(x => x[attr]);
  });

  // Flattens a sequence.
  eleventyConfig.addFilter("flat", function(sequence) {
    return sequence.flat();
  });

  // Returns a new sequence containing the unique elements of sequence.
  eleventyConfig.addFilter("unique", function(sequence) {
    return Array.from(new Set(sequence));
  });

  // Filters sequence, only retaining the elements x who include value.
  eleventyConfig.addFilter("includes", function(sequence, value) {
    return sequence.includes(value);
  });

  // Filters sequence, only retaining the elements x who include value.
  eleventyConfig.addFilter("sortByAttr", function(sequence, attr) {
    return sequence.sort((a, b) => a[attr] - b[attr]);
  });

  // Filters sequence, only retaining the elements x who include value.
  eleventyConfig.addFilter("sortByAttrAttr", function(sequence, attr1, attr2) {
    return sequence.sort((a, b) => a[attr1][attr2] - b[attr1][attr2]);
  });

  // Filters sequence, only retaining the elements x whose x[attr] includes value.
  eleventyConfig.addFilter("attrIncludes", function(sequence, attr, value) {
    return sequence.filter(x => x[attr].includes(value));
  });

  // Filters sequence, only retaining the elements x whose x[attr1][attr2] includes value.
  eleventyConfig.addFilter("attrAttrIncludes", function(sequence, attr1, attr2, value) {
    return sequence.filter(x => x[attr1][attr2].includes(value));
  });
     
  eleventyConfig.addPassthroughCopy("img");
  eleventyConfig.addPassthroughCopy("css");
  eleventyConfig.addPassthroughCopy("audio");

  return {
    dir: {
      output: "public"
    },
    pathPrefix: "/eoemi/",
    markdownTemplateEngine: "njk"
  };
};
